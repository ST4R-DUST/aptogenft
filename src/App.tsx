import React, { useEffect, useMemo, useState } from 'react';
import { WalletProvider, PontemWalletAdapter, AptosWalletAdapter, MartianWalletAdapter

} from '@manahippo/aptos-wallet-adapter';
import { Col, message, Row, Switch, Typography } from 'antd';
import Mint from './pages/Mint/';
const App: React.FC = () => {
  //const queryParams = new URLSearchParams(window.location.search);
  useEffect(() => {
    /*const url = new URL(window.location as any);
    url.searchParams.set('autoConnect', auto ? 'true' : 'false');
    window.history.pushState({}, '', url);*/
  }, [/*auto*/]);

  const wallets = useMemo(
    () => [
      new PontemWalletAdapter(),
      new AptosWalletAdapter(),
      new MartianWalletAdapter()
      //new BloctoWalletAdapter({ network: WalletAdapterNetwork.Testnet }),
    ],
    []
  );


  
  return (
    <WalletProvider
      wallets={wallets}
      onError={(error: Error) => {
        console.log('wallet errors: ', error);
        message.error(error.message);
      }}>
      <Mint />
    </WalletProvider>
  );
}

export default App;
