import styled from "styled-components";

export const StyledButton = styled("button")<any>`
  background: linear-gradient(90deg, rgba(0,112,121,1) 0%, rgba(0,236,255,1) 100%);
  color: ${(p) => (p.color ? "#fff" : "#fff")};
  font-size: 1rem;
  font-weight: 800;
  border: 0px solid #000;
  border-radius: 15px;
  padding: 13px 10px;
  cursor: pointer;
  box-shadow: 0px 0px 10px 0px rgba(0,236,255,1);

  

  &:hover{
    color: #fff;
    border: 0px solid #00DBDE;
    background: linear-gradient(90deg, rgba(0,112,121,0.8) 0%, rgba(0,236,255,0.8) 100%);
  }
`;
/*
-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0,255); 
  box-shadow: 0px 0px 10px 0px rgba(0, 0, 0,255);
  margin-top: 0.625rem;
  max-width: 200px;
  transition: all 0.3s ease-in-out;
  */
