import styled from "styled-components";

export const StyledAltButton = styled("button")<any>`
  background: ${(p) => p.color || "#263596"};
  color: ${(p) => (p.color ? "#fff" : "#fff")};
  font-size: 1rem;
  font-weight: 700;
  width: 100%;
  border: 4px solid #263596;
  border-radius: 15px;
  padding: 13px 0;
  cursor: pointer;
  box-shadow:0px 0px 10px 0px #263596;
  margin-top: 0.625rem;
  max-width: 180px;
  transition: all 0.3s ease-in-out;

  &:hover{
    color: #fff;
    border: 4px solid #2740db;
    background-color: #2740db;
    box-shadow:0px 0px 10px 0px #2740db;
  }
`;

