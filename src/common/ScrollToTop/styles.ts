import styled from "styled-components";

export const ScrollUpContainer = styled("div")<any>`
  padding: 10px;
  position: fixed;
  right: 30px;
  bottom: 30px;
  z-index: 10;
  cursor: pointer;
  border-radius:0px;
  border-width: 2px;
border-style: solid;
border-image: linear-gradient(to left, rgba(0,112,121,1), rgba(0,236,255,1)) 1;
  text-align: center;
  align-items: center;
  transition: all 0.3s ease-in-out;
  visibility: ${(p) => (p.show ? "visible" : "hidden")};
  opacity: ${(p) => (p.show ? "1" : "0")};
  display: flex;

  &:hover,
  &:active,
  &:focus {
  }

  @media screen and (max-width: 1240px) {
    display: none;
  }
`;
