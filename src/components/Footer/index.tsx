import Container from "../../common/Container";
import { Row, Col } from "antd";
import {
  DeskDiv,
  MobileDiv
} from "./styles";


const Footer = () => {
  return (
    <>
      <div style={{backgroundColor:"#000",color:"#fff",paddingTop:"36px",paddingBottom:"36px"}}>
        <Container>
        <DeskDiv>
        <Row justify="space-between">
          <Col>
            <Row style={{paddingBottom:"16px"}}>
              <img src="/logoalt.png" alt="logo.png" style={{display: "block",maxWidth:"200px",maxHeight:"120px",width:"auto",height:"auto",filter:"drop-shadow(0px 0px 10px #7cdbfe )"}}/>
            </Row>
            <Row >
            <p>© 2022, Aptoge</p>
            </Row>
        
          </Col>
          <Col>
              <Row style={{paddingBottom:"8px",justifyContent:"center",textAlign:"center"}}>
                <p>Join our socials!</p>
              </Row>
              <Row style={{justifyContent:"center"}}>

                <a style={{justifyContent:"center", paddingRight:"12px"}} href="https://twitter.com/AptogeAptos" target="_blank" rel="noreferrer">
                <Col>
                  
                    <img src="/img/twt.svg" alt="twt.svg"  style={{width:"48px",height:"48px", padding:"6px"}}/>
                  
                </Col>
                </a>
                <a style={{justifyContent:"center", paddingRight:"12px"}} href="https://t.me/Aptoge" target="_blank" rel="noreferrer">
                <Col>
                  
                    <img src="/img/tg.svg" alt="tg.svg"  style={{width:"48px",height:"48px", padding:"6px"}}/>
                  
                </Col>
                </a>

              </Row>


              
              
          </Col>
        </Row>
        </DeskDiv>
        <MobileDiv>
            <Row style={{paddingBottom:"16px",justifyContent:"center"}}>
              <img src="/logoalt.png" alt="logo.png" style={{maxWidth:"171px",maxHeight:"72px",width:"auto",height:"auto",filter:"drop-shadow(0px 0px 10px #7cdbfe )"}}/>
            </Row>
            <Row style={{justifyContent:"center"}}>
            <p>© 2022, Aptoge</p>
            </Row>
        

              <Row style={{paddingBottom:"0px",justifyContent:"center",textAlign:"center"}}>
                <p>Join our socials!</p>
              </Row>
              <Row style={{justifyContent:"center"}}>
                <a style={{justifyContent:"center", paddingRight:"12px"}} href="https://twitter.com/AptogeAptos" target="_blank" rel="noreferrer">
                <Col>
                  
                    <img src="/img/twt.svg" alt="twt.svg"  style={{width:"48px",height:"48px", padding:"6px"}}/>
                  
                </Col>
                </a>
                <a style={{justifyContent:"center", paddingRight:"12px"}} href="https://t.me/Aptoge" target="_blank" rel="noreferrer">
                <Col>
                  
                    <img src="/img/tg.svg" alt="tg.svg"  style={{width:"48px",height:"48px", padding:"6px"}}/>
                  
                </Col>
                </a>

              </Row>


        </MobileDiv>
        
        </Container>
      </div>

    </>
  );
};

export default (Footer);
