import styled from "styled-components";

export const FooterSection = styled("footer")`
  background: rgba(0,0,0,1);
  padding: 24px;
`;


export const LogoContainer = styled("div")`
  display: flex;
  position: relative;
`;

export const DeskDiv = styled("div")`
  @media only screen and (max-width: 1025px) {
    display: none;
  }
`;
export const MobileDiv = styled("div")`
  display:none;
  @media only screen and (max-width: 1025px) {
    display: block;
  }
`;
