import { useState } from "react";
import { Row, Col, Drawer } from "antd";
import Container from "../../common/Container";
import "antd/dist/antd.css";
import "./index.css";
import {
  HeaderSection,
  Burger,
  NotHidden,
  Menu,
  CustomNavLinkSmall,
  Label,
  Outline,
  Span,
  GlowImg,
} from "./styles";

function Header(){
  const [visible, setVisibility] = useState(false);

  
 /* const { active, account, library, connector, activate, deactivate } = useWeb3React()
  <Button onClick={connect} >Connect to MetaMask</Button>
      {active ? <p>Connected with {account}</p> : <p>no funca la wea</p>}
      <Button onClick={disconnect} >Disconnect</Button>
  */
  

  const showDrawer = () => {
    setVisibility(!visible);
  };

  const onClose = () => {
    setVisibility(false);
    window.scrollTo({top: 0});
  };

  const MenuItem = () => {
    const scrollTo = (id: string) => {
      const element = document.getElementById(id) as HTMLDivElement;
      element.scrollIntoView({
        behavior: "smooth",
      });
      setVisibility(false);
      
    };
    return (
      <>


        


      </>
    );
  };

  return (

    
    <HeaderSection style={{zIndex:5}} id="header">
      <Container>
        <Row justify="space-between" style={{}}>
          <Col span={4} >
          <a style={{justifyContent:"center", display:"flex"}} href="https://aptoge.com" target="_blank" rel="noreferrer">
              <GlowImg src="/logoalt.png" alt="logoalt.png" /> 
            </a>
          </Col>
          
          
          
            
            <NotHidden>
            <Col span={24} style={{textAlign:"center",alignItems:"center",justifyContent:"center"}}>
              <MenuItem />
              <CustomNavLinkSmall >
              <a style={{justifyContent:"center"}} href="https://www.topaz.so/collection/Aptoges-NFT-8883007341" target="_blank" rel="noreferrer">MARKETPLACE</a>
                </CustomNavLinkSmall>
                <CustomNavLinkSmall >
                <a href="https://twitter.com/AptogeAptos" target="_blank" rel="noreferrer">
                  <img src="/img/twt.svg" alt="twt.svg"  style={{width:"32px",height:"32px"}}/>
                </a>
                </CustomNavLinkSmall>
                <CustomNavLinkSmall >
                <a  href="https://t.me/Aptoge" target="_blank" rel="noreferrer">     
                      <img src="/img/tg.svg" alt="tg.svg"  style={{width:"32px",height:"32px"}}/>
                </a>
              </CustomNavLinkSmall>
              </Col>
              </NotHidden>
              
          
          
          
        </Row>
      </Container>
    </HeaderSection>
  );
};

export default Header;
