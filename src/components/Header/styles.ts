import styled from "styled-components";
import { MenuOutlined } from "@ant-design/icons";

export const HeaderSection = styled("header")`
  padding: 0.1rem 0.25rem;
  position: sticky;
  width: 100%;
  top: 0;
  .ant-row-space-between {
    align-items: center;
    text-align: center;
  }
  background: #000;
  
  
  

`;

export const LogoContainer = styled("div")`
  display: flex;
  font-weight: 600;
  text-align: center;
  
`;

export const GlowImg = styled("img")`
display: block;
max-width:171px;
max-height:72px;
width: auto;
height:auto;
filter:         drop-shadow(0px 0px 10px #7cdbfe ); 
}
`;

export const NavLink = styled("div")`
  display: inline-block;
  text-align: center;
`;

export const CustomNavLink = styled("div")`
  width: 140px;
  display: inline-block;
  

  @media only screen and (max-width: 411px) {
    width: 150px;
  }

  @media only screen and (max-width: 320px) {
    width: 118px;
  }
`;

export const ContactWrapper = styled("div")<any>`
  cursor: pointer;
  width: ${(p) => (p.width ? "100%" : "110px")};
  font-weight: 700;
  text-align: center;
  border-radius: 1.25rem;
  display: inline-block;
`;

export const Burger = styled("div")`
  @media only screen and (max-width: 1025px) {
    display: inline;
  }

  display: none;

  svg {
    fill: #18d6d3;
  }
`;

export const NotHidden = styled("div")`
  @media only screen and (max-width: 1025px) {
    display: none;
  }
`;

export const Menu = styled("h5")`
  font-size: 1rem;
  font-weight: 600;
  text-align: center;
`;

export const CustomNavLinkSmall = styled(NavLink)`
  font-size: 1.3rem;
  font-family:KanitBold;
  color: #18d6d3;
  transition: color 0.2s ease-in;
  margin-left:8px;
  margin-right:8px;
  @media only screen and (max-width: 768px) {

  }
`;

export const Label = styled("span")`
  font-weight: 500;
  color: #18d6d3;
  text-align: right;
  display: flex;
  justify-content: space-between;
  align-items: baseline;
`;

export const Outline = styled(MenuOutlined)<any>`
  font-size: 32px;
`;

export const Span = styled("span")`
  cursor: pointer;
  transition: all 0.3s ease-in-out;
  font-family:KanitBold;
  &:hover,
  &:active,
  &:focus {
    color: #fff;
    text-underline-position: under;
    text-decoration: #18ccf2 underline;
  }
`;
