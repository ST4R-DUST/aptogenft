import { Row, Col, Input, Select, DatePicker} from "antd";
import { Types } from 'aptos';
import { useWallet,} from '@manahippo/aptos-wallet-adapter';
import Slider from '@mui/material/Slider'
import { aptosDevnetClient, aptosMainnetClient} from '../../../config/aptosClient';
import { AptosAccount } from 'aptos';
import { Button } from "../../../common/Buttons/Button";
import { useState, useEffect, useMemo } from 'react';
import {
  LeftContentSection,
  CardWrap,
  ContentWrapper,
  DivButton,
} from "./styles";
import Countdown from "react-countdown";

/*

*/
export default function Home(){

  const [ isRelease, setIsRelease ] = useState(false);
  const releaseDate = new Date(1667350800000); 
  //const releaseDate = new Date(Date.now()+5000);
  const currentDate = new Date(Date.now())
  if(releaseDate <= currentDate)
  {
    //console.log(releaseDate)
    if(isRelease == false)
    {
      setIsRelease(true);
    }
    
  }
  
  const [txLoading, setTxLoading] = useState({
    sign: false,
    signTx: false,
    transaction: false,
    faucet: false
  });
 
  //Aptos Wallet
  const {
    connect,
    disconnect,
    account,
    signAndSubmitTransaction,
    wallets,
    connecting,
    connected,
    disconnecting,
    wallet: currentWallet,
    signMessage,
    signTransaction,
    network
  } = useWallet();

  const connectToPontem = async () => {
    setConnectId(0)
    const option = wallets[0]
    connect(option['adapter'].name)
    };

  const connectToPetra = async () => {
    setConnectId(1)
    const option = wallets[1]
    connect(option['adapter'].name)
    };

    const connectToMartian = async () => {
      setConnectId(2)
      const option = wallets[2]
      connect(option['adapter'].name)
      };

  const mintAddy = "0xc17197699c1a5c2d594bda3482e06cda9a7fe87378cbaec2b55aacd03e3f45d3";
  const venueAddy = "0x466ecd4bc0f71e6cd49112b6258a71ec2b58b5770d21751e157b250361731d8e";
  const resourceAddy = "0x88830073417a2e5f31b552249e7e99e293ef6ee7df3e29900df6d0023571a75";
  const collectionName = "Aptoges NFT"
   
  //Aptos hooks
  const [aptosClient, setAptosClient] = useState(aptosMainnetClient)
  const [connectId, setConnectId] = useState(-1)
  const [ isAptos, setIsAptos ] = useState(false);
  const [ isNetwork, setIsNetwork ] = useState(true);
  const [ isLoadingNetwork, setIsLoadingNetwork ] = useState(false);

  //hooks
  
  //const [ contract, setContract ] = useState<Contract | null>(null);
  const [ sliderVal, setSliderVal ] = useState(1);
  const [ mintVal, setMintVal ] = useState(1);
  const [ aptosMints, setAptosMints ] = useState(0);
  //Loading Mint section
  const [ isLoading1, setIsLoading1 ] = useState(false);
  //Loading Example bags
  const [ totalSupply, setTotalSupply ] = useState(0);
  const [ supplyLoaded, setSupplyLoaded ] = useState(false);
  const [ owned, setOwned ] = useState(0);



  useEffect(() => {
    if (typeof window.pontem !== 'undefined' || typeof window.aptos !== 'undefined' || typeof window.martian !== 'undefined' ) {
      setIsAptos(true);      
    }
    else
    {
      setIsAptos(false);
    }


    const id = setInterval(() => {
      if (typeof window.pontem !== 'undefined' || typeof window.aptos !== 'undefined' || typeof window.martian !== 'undefined') {
        setIsAptos(true);      
        if(connectId != -1)
        {
          
          //@ts-ignore
          if(network.name == 'Mainnet' || network.name === 'Aptos mainnet')// || network.name == 'Devnet' || network.name === 'Aptos devnet')
          {
              
              setIsNetwork(true);
              setIsLoadingNetwork(false);
              if(network.name === 'Mainnet' || network.name === 'Aptos mainnet' )
              {
                setAptosClient(aptosMainnetClient);
                fetchContractData(aptosMainnetClient)
              }
              /*if(network.name === 'Devnet' || network.name === 'Aptos devnet')
              {
                setAptosClient(aptosDevnetClient);
                fetchContractData(aptosDevnetClient)

              }*/
            
          }
          else
          {
            setIsNetwork(false);
            
          }
        }
        
      }
      else
      {
        setIsAptos(false);
      }
        
      
       /// <-- (3) invoke in interval callback
      }, 5000);
      // <-- (2) invoke on mount
  
    return () => clearInterval(id);
  }, [])

  //On network change
  useEffect(() => {
    if (typeof window.pontem !== 'undefined' || typeof window.aptos !== 'undefined' || typeof window.martian !== 'undefined' ) {
        setIsAptos(true);      

        if(connectId != -1)
        {
          //@ts-ignore
          if(network.name == 'Mainnet' || network.name === 'Aptos mainnet')// || network.name == 'Devnet' || network.name === 'Aptos devnet')
          {
            
              setIsNetwork(true);
              setIsLoadingNetwork(false);
              if(network.name === 'Mainnet' || network.name === 'Aptos mainnet' )
              {
                setAptosClient(aptosMainnetClient);
                fetchContractData(aptosMainnetClient)
              }
              /*if(network.name === 'Devnet' || network.name === 'Aptos devnet')
              {
                setAptosClient(aptosDevnetClient);
                fetchContractData(aptosDevnetClient)

              }*/
            
          }
          else
          {
            setIsNetwork(false);
          }
          /*.then(chainId => 
          {
    
            
          })*/
        }
      }
      
  }, [network])

  const fetchContractData = async (aptosClient) => {
      try{
        const mintResources = await aptosClient.getAccountResources(resourceAddy);
        let mintInfo = []
        for(var i = 0; i < mintResources.length;i++)
        {
          if((mintResources[i].type).includes("0x3::token::Collections"))
          {
            mintInfo = mintResources[i]
          }
        }
        //@ts-ignore
        setTotalSupply(mintInfo.data.mint_token_events.counter)
        setSupplyLoaded(true);
        //console.log(mintInfo)

      //Obtain price and total minted NFT

      //setMintVal(price);
      //setOwned(owned);
      //setTotalSupply(supply)
      //setIsLoading1(false);
      //setAptosMints(echMints);
    }
    catch(ex)
    {
      //console.log(ex)
    }
      
    }
    
  


  

  /**/

  const mintNFT = async () => {
    try {
      console.log(network);
      setTxLoading({
        ...txLoading,
        signTx: true
      });
      const txOptions = {
        max_gas_amount: '115000',
        gas_unit_price: '100'
      };
      if (account?.address || account?.publicKey) {
        const payload: Types.TransactionPayload = {
          type: 'entry_function_payload',
          function: mintAddy+'::candy_machine_v2::mint_tokens',
          //@ts-ignore FUCK TYPESCRIPT
          type_arguments: [],
          arguments: [
            //@ts-ignore FUCK TYPESCRIPT
            venueAddy,
            collectionName,
            sliderVal

            
          ]
        };
        await signAndSubmitTransaction(payload,txOptions);
      }
    } catch (err: any) {
      console.log('tx error: ', err.msg);
    } finally {
      setTxLoading({
        ...txLoading,
        signTx: false
      });
      
    }
    
  }


  const handleSliderChange = (event) => {
    setSliderVal(event.target.value);
  }

  //const isEnabled = true;

  return (
    <LeftContentSection id="intro" style={{textAlign:"center"}}>
      
      
       <Row justify="space-between" align="middle" >
          <Col lg={6} md={11} sm={12} xs={24}>
          <ContentWrapper>
            <img src={"/collection.gif"} alt="collection" width="256px" height="auto" />
            </ContentWrapper>
          </Col>
          <Col lg={10} md={11} sm={11} xs={24} >
            <ContentWrapper>
            <Row gutter={12} id='intro' style={{textAlign:'center'}}>
            <Col lg={24} md={24} sm={24} xs={24}>
            <h1 style={{textAlign:"center"}}>APTOGE <span style ={{color:"#efb75d"}}>|<br></br>NFT Mint</span></h1>
            </Col>
            </Row>
            <CardWrap>
            <p>All Aptoges NFT have been minted out!</p>
            <p>Check the <a style={{justifyContent:"center"}} href="https://www.topaz.so/collection/Aptoges-NFT-8883007341" target="_blank" rel="noreferrer">MARKETPLACE</a> to trade with others!</p>
            {/* 
            <p style={{fontSize:"24px"}}>1 Aptoge NFT costs <b>1 APT</b></p>
            {isRelease ? 
            <>
              {supplyLoaded ?
              <p style={{fontSize:"26px"}}>
                <b>{totalSupply}/1000 Minted</b>
              </p>
              :
              <></>
              }
              
              
              
              

                  
                {isAptos ?
                <>
                  {!isLoadingNetwork ?
                    <>
                      {isNetwork ?
                      <>
                        {connected ? 
                        <>
                          {!isLoading1 ?
                          <>
                          
                            <div style={{}}>
                            
                            
                              <p style={{fontSize:"20px"}}>
                                <b>Choose amount to mint:</b>
                              </p>
                              <p style={{width:"232px", display:"block", marginLeft:"auto", marginRight:"auto"}}>
                                <Slider
                                  aria-label="Small steps"
                                  defaultValue={1}
                                  step={1}
                                  marks
                                  min={1}
                                  max={10}
                                  color="primary"
                                  valueLabelDisplay="auto"
                                  onChange={handleSliderChange}
                                />
                              </p>
                              
                              
                              
                              
                              
                              <p style={{margin:"0px"}}>
                                <Button  onClick={() => {mintNFT()}}><b>MINT {sliderVal} APTOGES</b></Button>
                              </p>
                              
                              <p style={{fontSize:"18px", color:"#fff",paddingTop:"12px"}}><b>Mint Cost: <br/>{mintVal*sliderVal} APT</b></p>
                              {/*<p style={{fontSize:"18px"}}>
                                You own {owned} Aptoges NFT
                          </p>}
                            </div>

                          
                          
                          </>
                          :
                          <div  >
                            <div style={{}}>
                              <img src={`/loading.gif`} alt={'loading.gif'} width={'64px'} height={'64px'}/>
                            </div>
                          </div>
                          }
                          </>
                          
                        : 
                        <>
                        <Row gutter={24} style={{justifyContent:'center',paddingBottom:"20px"}}>

                        <DivButton style={{marginRight:"0px",marginBottom:"20px"}} onClick={() => {connectToPontem();}}><div   style={{color:"#fff",float:"left"}}>Connect Pontem Wallet </div><img style={{maxHeight:"32px",maxWidth:"32px",marginRight:"auto",float:"right"}} src="/img/pontem.svg" alt="pontem"/> </DivButton>
                        <DivButton style={{marginBottom:"20px"}}onClick={() => {connectToPetra();}}><div   style={{color:"#fff",float:"left"}}>Connect Petra Wallet </div><img style={{maxHeight:"24px",maxWidth:"24px",marginRight:"auto",float:"right"}} src="/img/petra.png" alt="pontem"/> </DivButton>
                        <DivButton onClick={() => {connectToMartian();}}><div   style={{color:"#fff",float:"left"}}>Connect Martian Wallet </div><img style={{maxHeight:"24px",maxWidth:"24px",marginRight:"auto",float:"right"}} src="/img/martian.png" alt="martian"/> </DivButton>
                        </Row>
                        </>
                      }
                      </>
                    :
                    <>
                        <div style={{}}>
                        
                        <p>
                            Wrong Network! Please switch over to the Aptos Mainnet.
                        </p>
                        </div>
                    </>
                    }
                    </>
                    :
                    <>
                      <div  >
                        <div style={{}}>
                          <p>
                            Checking Network...
                          </p>
                        </div>
                      </div>
                    </>
                  }
                </>
                :
                <>
                  <div >
                    <div style={{}}>
                    
                    <p>
                    Please install the <a href="https://chrome.google.com/webstore/detail/pontem-aptos-wallet/phkbamefinggmakgklpkljjmgibohnba" target="_blank" rel="noreferrer">Pontem Aptos Wallet</a> Browser Extension to use this dApp.
                    </p>
                    </div>
                  </div>
                </>
                }
            </>
            :
            <>
            <h6 style={{fontSize:"32px"}}>Release is in:</h6>
            <p style={{fontSize:"64px",margin:"0px"}}><Countdown zeroPadTime={2} onComplete={() => {setIsRelease(true)}} date={releaseDate} /></p>
            </>
            }
          */}  
              

            </CardWrap>  
            </ContentWrapper>
              
          </Col>
          <Col lg={6} md={11} sm={11} xs={24} >
          <ContentWrapper>
          <img src={"/collection2.gif"} alt="collection" width="256px" height="auto" />
          </ContentWrapper>
          </Col>
      </Row>
      
      
    </LeftContentSection>
  );
};

