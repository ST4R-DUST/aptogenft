import styled from "styled-components";

export const LeftContentSection = styled("section")`
  position: relative;
  padding: 1rem 0 1rem;
  min-height:100vh;
  @media only screen and (max-width: 1024px) {
    padding: 1rem 0 1rem;
  }
`;

export const ContentWrapper = styled("div")`
  position: relative;
  max-width: 540px;

  @media only screen and (max-width: 575px) {
    padding-bottom: 4rem;
  }
`;

export const ButtonWrapper = styled("div")`
  display: flex;
  justify-content: space-between;
  max-width: 100%;

  @media screen and (min-width: 1024px) {
    max-width: 80%;
  }

  button:last-child {
    margin-left: 20px;
  }
`;

export const Cimg = styled("img")`

  display: block;
  margin-left: auto;
  margin-right: auto;
`;

export const Content = styled("p")`
  margin: 1.5rem 0 2rem 0;
`;

export const HeaderSection = styled("div")`
  
  .ant-row{
    -webkit-box-shadow: 8px 8px 3px 0px rgba(0,0,0,0.2); 
    box-shadow: 8px 8px 3px 0px rgba(0,0,0,0.2);  
    align-items: center;
    text-align: center;
    margin-right:1%;
    margin-left:1%;
    background-color:#fff;
    border-radius:10px;
    border: 2px solid #000;
  }
  .ant-col
  {
    background-color:#fff;
    align-items: center;
    text-align: center;
    padding: 1rem 2rem;
    width:25%;
    cursor: pointer;
    font-size: 20px;
    
    &:hover,
    &:focus 
    {
      background-color: #dbdbdb;
    }
    @media only screen and (max-width: 426px) {
      font-size: 12px;
    }
    @media only screen and (max-width: 321px) {
      font-size: 10px;
    }
}
`;

export const ServiceWrapper = styled("div")`
  display: flex;
  justify-content: space-between;
  max-width: 100%;
`;

export const MinTitle = styled("h6")`
  font-size: 15px;
  line-height: 1rem;
  padding: 0.5rem 0;
  text-transform: uppercase;
  color: #000;
  font-family: "Motiva Sans Light", sans-serif;
`;

export const MinPara = styled("p")`
  font-size: 13px;
`;

export const DivButton = styled("div")`
  background: linear-gradient(90deg, rgba(0,112,121,1) 0%, rgba(0,236,255,1) 100%);
  color: ${(p) => (p.color ? "#fff" : "#fff")};
  font-size: 1rem;
  font-weight: 800;
  border-radius: 15px;
  cursor: pointer;
  padding: 13px 10px;
  box-shadow: 0px 0px 10px 0px rgba(0,236,255,1);
  

  &:hover{
    color: #fff;
    border: 0px solid #00DBDE;
    background: linear-gradient(90deg, rgba(0,112,121,0.8) 0%, rgba(0,236,255,0.8) 100%);
  }
`

export const CardWrap = styled("div")`
padding:2%;
border-radius:0px;
background-color: rgba(0,0,0,0);
text-align:center;
border-width: 4px;
border-style: solid;
border-image: linear-gradient(to left, rgba(0,112,121,1), rgba(0,236,255,1)) 1;
box-shadow: 0px 0px 15px 0px rgba(0,236,255,1);
font-size:1.2rem;
  `;