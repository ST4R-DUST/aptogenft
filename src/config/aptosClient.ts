import { AptosClient } from 'aptos';
import { MAINNET_NODE_URL, DEVNET_NODE_URL} from './aptosConstants';

export const aptosDevnetClient = new AptosClient(DEVNET_NODE_URL);
export const aptosMainnetClient = new AptosClient(MAINNET_NODE_URL);
