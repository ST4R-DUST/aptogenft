export const LOCAL_NODE_URL = 'http://127.0.0.1:8080';
export const DEV_NODE_URL = 'https://fullnode.devnet.aptoslabs.com/v1';
export const MAIN_NODE_URL = 'https://fullnode.mainnet.aptoslabs.com/v1';

export const DEVNET_NODE_URL = DEV_NODE_URL;

export const MAINNET_NODE_URL = MAIN_NODE_URL;
