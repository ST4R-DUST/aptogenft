import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import 'antd/dist/antd.css';
import { Styles } from './styles/styles';
import App from './App';
import reportWebVitals from './reportWebVitals';
const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
import Footer from './components/Footer';
import Header from './components/Header';

window.addEventListener('load', () => {
root.render(
  <React.StrictMode>
    
    <Styles />
    <Header/>
     
    <App />
    <Footer/>
  </React.StrictMode>
);
})
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
