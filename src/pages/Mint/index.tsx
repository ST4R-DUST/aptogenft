import { lazy } from "react";
import {
  BackgroundContainer
} from "./styles";


const Container = lazy(() => import("../../common/Container"));
const ScrollToTop = lazy(() => import("../../common/ScrollToTop"));
//const TokenomicsBlock = lazy(() => import("../../components/Home/TokenomicsBlock"));
//const PunkBlock = lazy(() => import ("../../components/Home/PunkBlock"));
const MintBlock = lazy(() => import ("../../components/Mint/MintBlock"));
//const ChartBlock = lazy(() => import ("../../components/Home/ChartBlock"));


const Mint = () => {
  return (
    <>
    <BackgroundContainer>

        <div style={{
              
        }}>
          <Container>
            <MintBlock/>
          </Container>
        </div>
            
              

        <ScrollToTop />
    </BackgroundContainer>
    </>
  );
};
export default Mint;
