import { createGlobalStyle } from 'styled-components';

export const Styles = createGlobalStyle`

    @font-face {
        font-family: "slkscreb";
        src: url("/fonts/slkscreb.ttf") format("truetype");
        font-style: normal;
    }

    @font-face {
        font-family: "slkscre";
        src: url("/fonts/slkscre.ttf") format("truetype");
        font-style: normal;
    }


    body,
    html,
    a {
        font-family: 'slkscre';
    }


    body {
        margin:0;
        padding:0;
        border: 0;
        outline: 0;
        background: #263596;
        overflow-x: hidden;
    }

    a:hover {
        color: #80509b;
        
    }

    input,
    textarea {
        border-radius: 4px;
        border: 0;
        background: rgb(241, 242, 243);
        transition: all 0.3s ease-in-out;  
        outline: none;
        width: 100%;  
        padding: 1rem 1.25rem;

        :focus-within {
            background: none;
        }
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: 'slkscreb';
        color:#fff;
        font-size: 48px;
        line-height: 1.18;
        z-index:-1

        @media only screen and (max-width: 890px) {
          font-size: 32px;
        }
      
        @media only screen and (max-width: 426px) {
          font-size: 32px;
        }
    }

    p {
        color: #fff;
        font-size: 24px;        
        line-height: 1.41;
        @media only screen and (max-width: 426px) {
            font-size: 16px;
          }
    }

    h1 {
        font-weight: 600;
        color:#fff;
    }

    a {
        text-decoration: none;
        outline: none;
        color: 
        #18d6d3;
        font-family:slkscreb;

        :hover {
            color: #00fffb;
        }
    }
    
    *:focus {
        outline: none;
    }

    .about-block-image svg {
        text-align: center;
    }

    .ant-drawer-body {
        display: flex;
        background-color: #282424;
        flex-direction: column;
        text-align: left;
        padding-top: 1.5rem;
    }

    .ant-drawer-content-wrapper {
        width: 300px !important;
    }
`;
